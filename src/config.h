
/**
<Copyright Header>
Copyright (c) 2013 Jordan "Earlz" Earls  <http://Earlz.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</Copyright Header>
*/

#ifndef LIGHTVM_CONFIG_H
#define LIGHTVM_CONFIG_H

//configuration directives

//#define SINGLE_MUL_DIV_SUPPORT //not yet supported


//options

/*
  Use this to give the greater world the middle finger and use
  optimizations that will make the VM environment non-portable.
*/
//#define SCREW_PORTABILITY

//Use this to specify that string "plain text" error messages are populated as well as an error code when something goes wrong
#define INCLUDE_ERROR_MESSAGES

//Specifies IP should be validated to be within our memory block.
//This makes the VM "safer", but also slower
//#define VALIDATE_IP_WITHIN_MEMORY

//Specifies that a big table should be used for storing XCall callbacks
//#define USE_BIG_XCALL_TABLE


#endif


