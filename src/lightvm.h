/**
<Copyright Header>
Copyright (c) 2013 Jordan "Earlz" Earls  <http://Earlz.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</Copyright Header>
**/

#ifndef LIGHTVM_H
#define LIGHTVM_H

#ifdef __cplusplus
extern "C" {
#endif

//standard includes required

#include <stdint.h>
#include <stdlib.h>


#define LIGHTVM_START_ADDRESS 64

//"convenience" typedefs
typedef uint16_t reg16_t;

//constants
#define REGISTER_IP 15
#define REGISTER_SP 14
#define REGISTER_TR 13
#define REGISTER_CR 12

#define OPCODE_8BIT 0x0080
#define OPCODE_ARG1_INDIRECT 0x0040
#define OPCODE_ARG2_INDIRECT 0x0020
#define OPCODE_HAS_IMMEDIATE 0x0010
#define OPCODE_ENCODE_ARG1(x) (x << 12)
#define OPCODE_ENCODE_ARG2(x) (x << 8)

//enumerations

typedef enum 
{
    LIGHTVM_NO_ERROR=0,
    /// Invalid/reserved opcode attempted to be executed
    LIGHTVM_INVALID_OPCODE=1,
    /// error status returned from xcall handler
    LIGHTVM_XCALL_ERROR=2,
    /// memory allocation has failed
    LIGHTVM_MEMORY_ALLOC_FAIL=3,
    /// you can't replace built-in xcalls
    LIGHTVM_BUILTIN_XCALL=4
}lightvm_error_t;



//structures

typedef struct
{
    uint_fast16_t begin, end; //beginning and ending addresses of memory
    void *memory;
    void *next;
}extended_memory_node;

struct xcall_node;

typedef struct
{
    uint8_t *memory; //primary memory
    uint_fast16_t memorysize; //primary memory size
    //pointers to the various special registers for ease of use
    reg16_t *ip;
    reg16_t *sp;
    reg16_t *tr;
    reg16_t *cr;
    reg16_t immdreg; //temporary invisible register
    
    const char *error;
    lightvm_error_t errorcode;
    extended_memory_node* extended;
    struct xcall_node* xcalls;
}lightvm_state;

/// returns 0 for success, otherwise will error the VM
typedef int (*xcall_t)(lightvm_state* s, int callnumber);
typedef struct xcall_node
{
    int callnumber;
    xcall_t call;
    struct xcall_node* next;
}xcall_node_t;


//intended public interface

/// Dumps the current state of the virtual machine to stdout using printf
void lightvm_dump(lightvm_state *s); //for when test-wat isn't enough
/// Initializes the virtual machine for use. Memory should be allocated and set before this point
int lightvm_init(lightvm_state* state);
/// Cleans up the xcall table/list
int lightvm_destroy_xcalls(lightvm_state* s);
/// Get the xcall function pointer from the xcall table for the given call number
xcall_t lightvm_get_xcall(lightvm_state* s, int callnumber);
/// Get a pointer to the memory of an 8bit register
uint8_t *lightvm_8bit_register(lightvm_state* state, unsigned int reg);
/// Get a pointer to the memory of a register
reg16_t *lightvm_register(lightvm_state* state, unsigned int reg);
/// Add an xcall function to the virtual machine 
int lightvm_add_xcall(lightvm_state* s, int callnumber, xcall_t callback);
/// Pops a value off of the stack of the virtual machine
uint16_t lightvm_pop(lightvm_state* s);
/// Returns the value off of the stack of the virtual machine without modifying the stack register
uint16_t lightvm_peek(lightvm_state *s);
/// Push a value onto the stack of the virtual machine
void lightvm_push(lightvm_state* s, uint16_t value);
/// Performs a single instruction. Returns 0 if no error, otherwise check the error and errorcode values of the state
int lightvm_step(lightvm_state* s); //provide just a step function. Just put this in a loop

//probably internal prototypes
/// Reads a single byte from the current location of IP and increments IP by 1
uint8_t lightvm_next_op_byte(lightvm_state* s);
/// Reads a single word from the current location of IP and increments IP by 2
uint16_t lightvm_next_op_word(lightvm_state* s);
/// Parses the first argument in 8bit mode. Returns a pointer to the value of the argument
uint8_t *parse_8bit_arg1(lightvm_state* s, uint16_t op);
/// Parses the second argument in 8bit mode. Returns a pointer to the value of the argument. Might increment IP as a side effect
uint8_t *parse_8bit_arg2(lightvm_state* s, uint16_t op);
/// Parses the first argument in 16bit mode. Returns a pointer to the value of the argument
uint16_t *parse_16bit_arg1(lightvm_state* s, uint16_t opcode);
/// Parses the second argument in 16bit mode. Returns a pointer to the value of the argument. Might increment IP as a side effect
uint16_t *parse_16bit_arg2(lightvm_state* s, uint16_t opcode);
/// Actually performs the operation given the op argument and the 2 argument values passed in
int lightvm_step_8bit(lightvm_state* s, uint16_t op, uint8_t* arg1, uint8_t* arg2);
/// Actually performs the operation given the op argument and the 2 argument values passed in
int lightvm_step_16bit(lightvm_state* s, uint16_t op, uint16_t* arg1, uint16_t* arg2);
/// Helper method to perform a short branch instruction
int branch_short(lightvm_state* s, unsigned int primary, uint8_t arg);
/// Helper method to perform the various comparison instructions
void op16_compares(lightvm_state* s, uint16_t op, uint16_t* arg2);
/// Helper method to perform the various single argument instructions
void op16_single_arg(lightvm_state* s, uint16_t op, uint16_t* arg2);

int lightvm_check_xcall_status_opcode(lightvm_state* s, int callnumber);

#ifdef __cplusplus
}
#endif

#endif
