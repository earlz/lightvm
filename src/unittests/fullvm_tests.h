/**
<Copyright Header>
Copyright (c) 2013 Jordan "Earlz" Earls  <http://Earlz.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</Copyright Header>
**/

/* Yes, I know this is horrible terrible practice, but it's just unit tests, so I'll let it slide */
/* These are what I would consider "integration" tests. They test the entire VM.
   I've determined that this is the easiest way to test opcodes without relying on "undefined" behavior
   nor without many lines of setup for each test case
   Also, it's fun writing instructions in hex :P
*/

#define INIT_VM() lightvm_state s; s.memory=malloc(256); s.memorysize=256; lightvm_init(&s)
#define FREE_VM() free(s.memory)

uint8_t test_storage_code[] = {
//0
0x15, 0x00, 0x34, 0x12, //mv r0, 0x1234  
0x15, 0x50, 0xF1, 0xF2, //mv r5, 0x1234  
//8
0x15, 0xF0, 64, 0 //mv ip, 64           
//12

//0x00 //nop
};



MU_TEST(some_opcodes) 
{
    INIT_VM();
    memcpy(&s.memory[64], test_storage_code, sizeof(test_storage_code));
    
    lightvm_step(&s);
    lightvm_dump(&s);
    mu_check(*lightvm_register(&s, 0)==0x1234);
    mu_check(*(s.ip)==64+4);
    lightvm_step(&s);
    mu_check(*(s.ip)==64+8);
    mu_check(*lightvm_register(&s, 5)==0xF2F1);
    lightvm_step(&s);
    mu_check(*(s.ip)==64);
    
    
    FREE_VM();
}

