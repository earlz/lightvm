
/**
<Copyright Header>
Copyright (c) 2013 Jordan "Earlz" Earls  <http://Earlz.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</Copyright Header>
**/

#include "../lightvm.h"
#include "minunit.h"
#include <stdlib.h>

#include "fullvm_tests.h"

void init_lightvm_test(lightvm_state *s){
    s->memory=malloc(100);
    s->memorysize=100;
    lightvm_init(s);
}

MU_TEST(register_returns_null_when_invalid) {
    lightvm_state s;
    mu_check(lightvm_register(&s, 17)==NULL);
    mu_check(lightvm_register(&s, -1)==NULL);
}

MU_TEST(register_returns_proper_address)
{
    lightvm_state s;
    s.memory=malloc(100);
    s.memorysize=100;
    intptr_t reg0=lightvm_register(&s, 0);
    intptr_t reg1=lightvm_register(&s, 1);
    mu_check(reg0 == reg1-2);
    mu_check(reg0 == (intptr_t)s.memory);
    mu_check(reg0 == &s.memory[0]);
    mu_check(reg1 == &s.memory[2]);
    free(s.memory);
}

MU_TEST(init_returns_error_on_invalid_or_small_memory)
{
    lightvm_state s;
    s.memory=NULL;
    mu_check(lightvm_init(&s)==-1);
    s.memory=malloc(5);
    s.memorysize=5;
    mu_check(lightvm_init(&s)==-1);
    free(s.memory);
}
MU_TEST(init_initializes_registers_correctly)
{
    mu_check(sizeof(reg16_t)<=4);
    lightvm_state s;
    uint8_t *m=malloc(100);
    s.memory=m;
    s.memorysize=100;
    for(int i=0;i<s.memorysize;i++)
    {
        m[i]=i; //randomize memory
    }
    lightvm_init(&s);
    for(int i=0;i<14;i++)
    {
        mu_check(*lightvm_register(&s, i)==0);
    }
    mu_check(*lightvm_register(&s, REGISTER_IP)==64);
    mu_check(*lightvm_register(&s, REGISTER_SP)==s.memorysize);
    
    mu_check(s.ip == lightvm_register(&s, REGISTER_IP));
    
}

MU_TEST(next_op_goes_to_proper_address)
{
    lightvm_state s;
    s.memory=malloc(100);
    s.memorysize=100;
    s.memory[64]=0xF1;
    *(uint16_t*)&s.memory[65]=0x1234;
    lightvm_init(&s);
    int tmp=*s.ip;
    
    mu_check(lightvm_next_op_byte(&s)==0xF1);
    mu_check(lightvm_next_op_word(&s)==0x1234);
    mu_check(*s.ip=tmp+3);
}


MU_TEST(parse_16bit_arg1_is_correct)
{
    lightvm_state s;
    s.memory=malloc(100);
    s.memorysize=100;
    lightvm_init(&s);
    *lightvm_register(&s, 5) = 0x1234;
    uint16_t op=0x5040; //register is indirect and 5 is register number
    uint16_t* tmp=parse_16bit_arg1(&s, op);
    mu_check(tmp == (&s.memory[0x1234]));
    op=0xF000; //register is direct and IP is register
    tmp=parse_16bit_arg1(&s, op);
    mu_check(*tmp == 64);
}

MU_TEST(parse_16bit_arg2_is_correct)
{
    lightvm_state s;
    s.memory=malloc(100);
    s.memorysize=100;
    lightvm_init(&s);
    *lightvm_register(&s, 5) = 0x1234;
    uint16_t op=0x0520; //register is indirect and 5 is register number
    uint16_t* tmp=parse_16bit_arg2(&s, op);
    mu_check(tmp == (&s.memory[0x1234]));
    op=0x0F00; //register is direct and IP is register
    tmp=parse_16bit_arg2(&s, op);
    mu_check(*tmp == 64);
    
    //now test immediates
    *(uint16_t*)&s.memory[64] = 0x4321;
    op=0x0010; //immediate direct
    tmp=parse_16bit_arg2(&s, op);
    mu_check(*tmp == 0x4321);
    mu_check(tmp == &s.immdreg); //ensure points to invisible register and not to code stream
    
    *(uint16_t*)&s.memory[66] = 0x0FF1; //get ready for next immediate
    op=0x0030; //immediate indirect
    tmp=parse_16bit_arg2(&s, op);
    mu_check(tmp == (&s.memory[0x0FF1]));
}


MU_TEST(parse_8bit_arg1_is_correct)
{
    lightvm_state s;
    s.memory=malloc(100);
    s.memorysize=100;
    lightvm_init(&s);
    *lightvm_register(&s, 5) = 0x1234;
    uint16_t op=OPCODE_8BIT | OPCODE_ARG1_INDIRECT | OPCODE_ENCODE_ARG1(5); //register is indirect and 5 is register number
    uint8_t* tmp=parse_8bit_arg1(&s, op);
    mu_check(tmp == (&s.memory[0x1234]));
    op=OPCODE_8BIT | OPCODE_ENCODE_ARG1(5); //register is direct and 5 is register
    tmp=parse_8bit_arg1(&s, op);
    mu_check(tmp == lightvm_8bit_register(&s, 5)); 
    mu_check(tmp == &s.memory[5]);
    //check that register endianness is correct
    mu_check(*lightvm_8bit_register(&s, 10) == 0x34);
    mu_check(*lightvm_8bit_register(&s, 11) == 0x12);
}


MU_TEST(parse_8bit_arg2_is_correct)
{
    lightvm_state s;
    s.memory=malloc(100);
    s.memorysize=100;
    lightvm_init(&s);
    *lightvm_register(&s, 5) = 0x1234;
    uint16_t op=OPCODE_8BIT | OPCODE_ARG2_INDIRECT | OPCODE_ENCODE_ARG2(5); //register is indirect and 5 is register number
    uint8_t* tmp=parse_8bit_arg2(&s, op);
    mu_check(tmp == (&s.memory[0x1234]));
    op=OPCODE_8BIT | OPCODE_ENCODE_ARG2(5); //register is direct and IP is register
    tmp=parse_8bit_arg2(&s, op);
    mu_check(tmp == lightvm_8bit_register(&s, 5)); 
    mu_check(tmp == &s.memory[5]);
    
    //now test immediates
    //step hasn't been executed so IP is still at START
    *(uint16_t*)&s.memory[LIGHTVM_START_ADDRESS] = 0x4321; 
    op=OPCODE_8BIT | OPCODE_HAS_IMMEDIATE; //immediate direct
    tmp=parse_8bit_arg2(&s, op);
    mu_check(*tmp == 0x21);
    mu_check(tmp == &s.immdreg); //ensure points to invisible register and not to code stream

    //parsing will do a step with immediates, so advance by 2
    *(uint16_t*)&s.memory[LIGHTVM_START_ADDRESS+2] = 0x0FF1; //get ready for next immediate
    op=OPCODE_8BIT | OPCODE_HAS_IMMEDIATE | OPCODE_ARG2_INDIRECT; //immediate indirect
    tmp=parse_8bit_arg2(&s, op);
    mu_check(tmp == (&s.memory[0x0FF1]));

}



MU_TEST(branch_short_properly_changes_ip)
{
    lightvm_state s;
    s.memory=malloc(100);
    s.memorysize=100;
    lightvm_init(&s);
    branch_short(&s, 12, 50);
    
    mu_check(*s.ip == 64+50);
    *s.ip = 64;
    branch_short(&s, 12, -50);
    mu_check(*s.ip == 64-50);
}


MU_TEST(signed_comparisons_are_correct)
{
    lightvm_state s;
    s.memory=malloc(100);
    s.memorysize=100;
    lightvm_init(&s);
    
    //ensure CR is treated as signed
    *s.tr=0;
    *s.cr=(uint16_t)-1;
    uint16_t op=0x9000; //or.lt.s
    int16_t arg=2;
    op16_compares(&s, op, &arg); 
    mu_check(*s.tr > 0); 
    
    //ensure arg is treated as signed
    *s.tr=0;
    *s.cr=(uint16_t)1;
    op=0xD000; //or.gt.s
    arg= -1;
    op16_compares(&s, op, &arg); 
    mu_check(*s.tr > 0); 
}

#ifdef single_mul_div_support
MU_TEST(single_mul_test){
    lightvm_state s;
    init_lightvm_test(&s);
    *lightvm_register(&s, 0) = 5;
    *lightvm_register(&s, 1) = 10;
    sinlge_mul(&s, 0);
    mu_check(*lightvm_register(&s) == 50);

}
#endif


MU_TEST_SUITE(test_suite) {
    MU_RUN_TEST(register_returns_null_when_invalid);
    MU_RUN_TEST(register_returns_proper_address);
    MU_RUN_TEST(init_returns_error_on_invalid_or_small_memory);
    MU_RUN_TEST(init_initializes_registers_correctly);
    MU_RUN_TEST(next_op_goes_to_proper_address);
    MU_RUN_TEST(parse_16bit_arg1_is_correct);
    MU_RUN_TEST(parse_16bit_arg2_is_correct);
    MU_RUN_TEST(signed_comparisons_are_correct);
    MU_RUN_TEST(parse_8bit_arg1_is_correct);
    MU_RUN_TEST(parse_8bit_arg2_is_correct);
    
    MU_RUN_TEST(some_opcodes);


#ifdef single_mul_div_support
    MU_RUN_TEST(single_mul_test);
#endif
    
}


int main(int argc, char *argv[]) {
    MU_RUN_SUITE(test_suite);
    MU_REPORT();
    return 0;
}

