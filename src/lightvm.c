/**
<Copyright Header>
Copyright (c) 2013 Jordan "Earlz" Earls  <http://Earlz.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</Copyright Header>
**/

#include "lightvm.h"
#include "config.h"
#include "stdio.h"

int lightvm_init(lightvm_state* state)
{
    if(state->memory==NULL || state->memorysize<64)
    {
        return -1;
    }
    for(int i=0;i<15;i++)
    {
        *lightvm_register(state, i)=0;
    }
    state->errorcode = LIGHTVM_NO_ERROR;
    state->error = "";
    state->ip=lightvm_register(state, REGISTER_IP);
    state->sp=lightvm_register(state, REGISTER_SP);
    state->tr=lightvm_register(state, REGISTER_TR);
    state->cr=lightvm_register(state, REGISTER_CR);
    *state->ip=LIGHTVM_START_ADDRESS; //set to end of register block
    *state->sp=state->memorysize; //set to end of memory(stack grows downwards)
    state->xcalls=NULL;
 
    return 0;
}

int lightvm_destroy(lightvm_state* s)
{
    lightvm_destroy_xcalls(s);
    return 0;
}

uint16_t* memory_word(lightvm_state* s, reg16_t address)
{
    return (uint16_t*)&s->memory[address];   
}
uint8_t* memory_byte(lightvm_state* s, reg16_t address)
{
    return (uint8_t*)&s->memory[address];
}

reg16_t *lightvm_register(lightvm_state* state, unsigned int reg)
{
    if(reg>15)
    {
        return NULL;
    }
    return (reg16_t*) memory_word(state, reg<<1); //shortcut for reg*2
}

uint8_t *lightvm_8bit_register(lightvm_state* state, unsigned int reg)
{
    if(reg>15)
    {
        return NULL;
    }
    return (uint8_t*) memory_word(state, reg); 
}


uint8_t lightvm_next_op_byte(lightvm_state* s)
{
    uint8_t tmp=*memory_byte(s, *s->ip);
    (*s->ip)++;
    return tmp;
}
uint16_t lightvm_next_op_word(lightvm_state* s)
{
    uint16_t tmp=*memory_word(s, *s->ip);
    (*s->ip)+=2;
    return tmp;
}

void lightvm_set_error(lightvm_state* s, lightvm_error_t err)
{
    s->errorcode=err;
    #ifdef INCLUDE_ERROR_MESSAGES
    switch(err)
    {
        case LIGHTVM_NO_ERROR:
        s->error="No error has occured!?";
        break;
        case LIGHTVM_INVALID_OPCODE:
        s->error="Invalid Opcode";
        break;
        case LIGHTVM_XCALL_ERROR:
        s->error = "Error returned from Xcall handler";
        break;
        case LIGHTVM_MEMORY_ALLOC_FAIL:
        s->error = "A memory allocation failed";
        break;
        case LIGHTVM_BUILTIN_XCALL:
        s->error = "Can't replace built in Xcalls";
        break;
        default:
        s->error="Unknown error";
        break;
    }
    #endif
}

int lightvm_step(lightvm_state* s)
{
    uint16_t op=lightvm_next_op_word(s);
    unsigned int primary = (op&0x00FF) >> 8;
    //bitfields sound nice, but not the most portable things to keep exact to a bit.
    unsigned int _8bit=(primary&0x80) > 0;
  //  unsigned int arg1_ptr = op&0x4000 > 0;
  //  unsigned int arg2_ptr = op&0x2000 > 0;
  //  unsigned int immediate = op&0x1000 > 0;
    unsigned int second = op &0x00FF;
    
    //gather args
    
    
    if(_8bit && primary>=12 && primary<=15)
    {
        //is branch.short
        return branch_short(s, primary, second);
    }
    if(_8bit)
    {
        uint8_t *arg1=parse_8bit_arg1(s, op);
        uint8_t *arg2=parse_8bit_arg2(s, op);
        if(lightvm_step_8bit(s, op, arg1, arg2) != 0){
            lightvm_set_error(s, LIGHTVM_INVALID_OPCODE); //probably should do something more robust sometime down the line..
            return -1;
        }
    }
    else
    {
        uint16_t *arg1=parse_16bit_arg1(s, op);
        uint16_t *arg2=parse_16bit_arg2(s, op);
        if(lightvm_step_16bit(s, op, arg1, arg2) != 0){
            lightvm_set_error(s, LIGHTVM_INVALID_OPCODE);
            return -1;
        }
    }
    if(s->errorcode != 0){
        return s->errorcode;
    }
    return 0;
}

uint16_t *parse_16bit_arg1(lightvm_state* s, uint16_t op)
{
    unsigned int arg1_ptr = (op&0x0040) > 0;
    unsigned int reg=(op & 0xF000) >> 12;
    if(arg1_ptr)
    {
        return memory_word(s, *lightvm_register(s, reg));
    }
    return (uint16_t*)lightvm_register(s, reg);
}
uint16_t *parse_16bit_arg2(lightvm_state* s, uint16_t op)
{
    unsigned int arg2_ptr = (op&0x0020) > 0;
    unsigned int isimmediate = (op&0x0010) > 0;
    unsigned int reg=(op& 0x0F00) >> 8;
    uint16_t* tmp;
    if(isimmediate)
    {
        uint16_t immd=lightvm_next_op_word(s);
        s->immdreg=immd;
        tmp=(uint16_t*)&s->immdreg;
    }
    else
    {
        tmp=(uint16_t*)lightvm_register(s, reg);
    }
    
    if(arg2_ptr)
    {
        return memory_word(s, *tmp);
    }
    return tmp;
}
uint8_t *parse_8bit_arg1(lightvm_state* s, uint16_t op)
{
    unsigned int arg1_ptr = (op&0x0040) > 0;
    unsigned int reg=(op & 0xF000) >> 12;
    if(arg1_ptr)
    {
        return memory_byte(s, *lightvm_register(s, reg));
    }
    return lightvm_8bit_register(s, reg);
}
uint8_t *parse_8bit_arg2(lightvm_state* s, uint16_t op)
{
    unsigned int arg2_ptr = (op&0x0020) > 0;
    unsigned int isimmediate = (op&0x0010) > 0;
    unsigned int reg=(op& 0x0F00) >> 8;
    uint8_t* tmp;
    if(isimmediate)
    {
        uint16_t immd=lightvm_next_op_word(s);
        s->immdreg=immd;
        tmp=(uint8_t*)&s->immdreg;
        if(arg2_ptr)
        {
            return memory_byte(s, s->immdreg);
        }
    }
    else
    {
        if(arg2_ptr)
        {
            return memory_byte(s, *lightvm_register(s, reg));   
        }else
        {
            tmp=lightvm_8bit_register(s, reg);
        }
    }
    
    if(arg2_ptr)
    {
        return memory_byte(s, *tmp);
    }
    return tmp;
}

void lightvm_push(lightvm_state* s, uint16_t value)
{
    (*s->sp)-=2;
    *memory_word(s, *s->sp) = value;
}
uint16_t lightvm_pop(lightvm_state* s)
{
    uint16_t tmp = *memory_word(s, *s->sp);
    (*s->sp)+=2;
    return tmp;
}

uint16_t lightvm_peek(lightvm_state *s)
{
    uint16_t tmp = *memory_word(s, *s->sp);
    return tmp;
}

int lightvm_step_8bit(lightvm_state* s, uint16_t op, uint8_t* arg1, uint8_t* arg2)
{
    unsigned int primary=op & 0x0F00 >> 8;
    switch(primary)
    {
        case 0:
        //1-arg ops
            //op16_single_arg(s, op, arg2);
            break;
        case 1:
        //xor
            *arg1=(*arg1) ^ (*arg2);
            break;
        case 2:
        //or
            *arg1=(*arg1) | (*arg2);
            break;
        case 3:
        //and
            *arg1=(*arg1) & (*arg2);
            break;
        case 4:
        //unused
            break;
        case 5:
        //mv
            *arg1=*arg2;
            break;
        case 6:
        //comparison 1-arg
            break;
        case 7:
        //mv.iftrue
            if(*s->tr)
            {
                *arg1=*arg2;
            }
            break;
        case 8:
        //shl
            *arg1 = (*arg1) << (*arg2);
            break;
        case 9:
        //shr
            *arg1 = (*arg1) >> (*arg2);
            break;
        case 10:
        //add
            *arg1 = (*arg1) + (*arg2);
            break;
        case 11:
        //sub
            *arg1 = (*arg1) + (*arg2);
            break;
        case 12:
        //push.mv
            lightvm_push(s, *arg1);
            *arg1 = *arg2;
            break;
        case 13:
        //push.mv.ifture
            if(*s->tr)
            {
                lightvm_push(s, *arg1);
                *arg1 = *arg2;
            }
            break;
        case 14:
        //push.mv.iffalse
            if(!(*s->tr))
            {
                lightvm_push(s, *arg1);
                *arg1 = *arg2;
            }
        case 15:
        //extensions/unused
            if((op & 0x00FF) == 0)
            {
                lightvm_dump(s);
            }
        default:
        return -1; //shouldn't reach here
    }
    return 0;
}

void lightvm_dump(lightvm_state *s) //for when test-wat isn't enough
{
    for(int i=0;i<16;i++)
    {
        reg16_t tmp=*lightvm_register(s, i);
        printf("r%i = %X\n", i, tmp);
    }
}


int lightvm_step_16bit(lightvm_state* s, uint16_t op, uint16_t* arg1, uint16_t* arg2)
{
    unsigned int primary=op & 0x0F00 >> 8;
    switch(primary)
    {
        case 0:
        //1-arg ops
            op16_single_arg(s, op, arg2);
            break;
        case 1:
        //xor
            *arg1=(*arg1) ^ (*arg2);
            break;
        case 2:
        //or
            *arg1=(*arg1) | (*arg2);
            break;
        case 3:
        //and
            *arg1=(*arg1) & (*arg2);
            break;
        case 4:
        //unused
            break;
        case 5:
        //mv
            *arg1=*arg2;
            break;
        case 6:
        //comparison 1-arg
            op16_compares(s, op, arg2);
            break;
        case 7:
        //mv.iftrue
            if(*s->tr)
            {
                *arg1=*arg2;
            }
            break;
        case 8:
        //shl
            *arg1 = (*arg1) << (*arg2);
            break;
        case 9:
        //shr
            *arg1 = (*arg1) >> (*arg2);
            break;
        case 10:
        //add
            *arg1 = (*arg1) + (*arg2);
            break;
        case 11:
        //sub
            *arg1 = (*arg1) + (*arg2);
            break;
        case 12:
        //push.mv
            lightvm_push(s, *arg1);
            *arg1 = *arg2;
            break;
        case 13:
        //push.mv.ifture
            if(*s->tr)
            {
                lightvm_push(s, *arg1);
                *arg1 = *arg2;
            }
            break;
        case 14:
        //push.mv.iffalse
            if(!(*s->tr))
            {
                lightvm_push(s, *arg1);
                *arg1 = *arg2;
            }
        case 15:
        //extensions/unused
            if((op & 0x00FF) == 0)
            {
                lightvm_dump(s);
            }
        default:
        return -1; //shouldn't reach here
    }
    return 0;
}

int branch_short(lightvm_state* s, unsigned int primary, uint8_t arg)
{
    unsigned int op = primary & 0x0F;
    if(op==13 && !*s->tr) //if opcode is br.iftrue and TR is false, then don't branch
    {
        return 0;
    }
    if(op==14 && *s->tr) //if opcode is br.iffalse and TS is true, then don't branch
    {
        return 0;
    }
    //you can never be too explicit when mixing signed and unsigned values
    int16_t diff = (int8_t)arg; //make it so it can go +/-256 instead of 128
    diff=diff << 1;
    int16_t ip=*s->ip;
    ip=ip+diff;
    *s->ip=*(uint16_t*)&ip; //make sure not to "cast" the int16 to uint16. Just copy direct. Assume C compiler is 2's complement
    return 0;
}
int lightvm_add_xcall(lightvm_state* s, int callnumber, xcall_t callback)
{
    if(callnumber >= 0x8000 && callnumber <= 0x9000){
        //reserved for built-in special handling
        return -1;
    }
    xcall_node_t* tmp=s->xcalls;
    s->xcalls=malloc(sizeof(xcall_node_t));
    if(s->xcalls==NULL)
    {
        s->xcalls=tmp;
        return -1;
    }
    s->xcalls->callnumber=callnumber;
    s->xcalls->call=callback;
    s->xcalls->next=tmp;
    return 0;
}

int lightvm_destroy_xcalls(lightvm_state* s)
{
    while(s->xcalls!=NULL)
    {
        xcall_node_t* tmp;
        tmp=s->xcalls;
        s->xcalls=s->xcalls->next;
        free(tmp);
    }
    return 0;
}
xcall_t lightvm_get_xcall(lightvm_state* s, int callnumber)
{
    //do built-ins.. 
    if(callnumber >= 0x8000 && callnumber <=0x800F){
        return lightvm_check_xcall_status_opcode;
    }
    xcall_node_t* item=s->xcalls;
    while(item!=NULL)
    {
        if(item->callnumber == callnumber)
        {
            return item->call;
        }
        item=item->next;
    }
    return NULL;
}



int lightvm_check_xcall_status_opcode(lightvm_state* s, int callnumber)
{
    int reg=callnumber - 0x8000; 
    if(reg > 16 || reg < 0){
        return 1;
    }
    *lightvm_register(s, reg) = lightvm_get_xcall(s, *lightvm_register(s, reg)) != NULL;
    return 0;
}













