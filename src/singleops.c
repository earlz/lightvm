/**
<Copyright Header>
Copyright (c) 2013 Jordan "Earlz" Earls  <http://Earlz.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</Copyright Header>
**/

#include "lightvm.h"

void op16_single_arg(lightvm_state* s, uint16_t op, uint16_t* arg2)
{
    unsigned int secondary=(op & 0xF000) >> 12;
    switch(secondary)
    {
        case 0:
        //push
        lightvm_push(s, *arg2);
        break;
        case 1:
        //pop
        *arg2 = lightvm_pop(s);
        break;
        case 2:
        //not
        *arg2 = ~(*arg2);
        break;
        case 3:
        //unused
        break;
        case 4:
        //unused
        break;
        case 5:
        //unused
        break;
        case 6:
        {
            xcall_t call=lightvm_get_xcall(s, *arg2);
            if(call!=NULL)
            {
                if(call(s, *arg2) != 0)
                {
                    lightvm_set_error(s, LIGHTVM_XCALL_ERROR);
                }
            }
        }
        break;
        //xcall?
        case 7:
        //and.eq
        *s->tr &= *s->cr == *arg2;
        break;
        case 8:
        //or.eq
        *s->tr |= *s->cr == *arg2;
        break;
        case 9:
        //and.neq
        *s->tr &= *s->cr != *arg2;
        break;
        case 10:
        //or.neq
        *s->tr |= *s->cr != *arg2;
        break;
        case 11:
        //set TR
        *s->tr = 1;
        break;
        case 12:
        //reset TR
        *s->tr = 0;
        break;
        case 13:
        //load CR
        *s->cr = *arg2;
        break;
        case 14:
        //8.bank PROBABLY NOT
        case 15:
        //unused
        default:
        return;
    }
    return;
}
void op16_compares(lightvm_state* s, uint16_t op, uint16_t* arg2)
{
    unsigned int secondary=(op & 0xF000) >> 12;
    int16_t sr=(int16_t)*arg2;
    int16_t sl=(int16_t)*s->cr;
    
    switch(secondary)
    {
        case 0:
        //and.lt
        *s->tr &= *s->cr < *arg2;
        break;
        case 1:
        //or.lt
        *s->tr |= *s->cr < *arg2;
        break;
        case 2:
        //and.lteq
        *s->tr &= *s->cr <= *arg2;
        break;
        case 3:
        //or.lteq
        *s->tr |= *s->cr <= *arg2;
        break;
        case 4:
        //and.gt
        *s->tr &= *s->cr > *arg2;
        break;
        case 5:
        //or.gt
        *s->tr |= *s->cr > *arg2;
        break;
        case 6:
        //and.gteq
        *s->tr &= *s->cr >= *arg2;
        break;
        case 7:
        //or.gteq
        *s->tr |= *s->cr >= *arg2;
        break;
        case 8:
        //and.lt.s
        *s->tr &= sl < sr;
        break;
        case 9:
        //or.lt.s
        *s->tr |= sl < sr;
        break;
        case 10:
        //and.lteq.s
        *s->tr &= sl <= sr;
        break;
        case 11:
        //or.lteq.s
        *s->tr |= sl <= sr;
        break;
        case 12:
        //and.gt.s
        *s->tr &= sl > sr;
        break;
        case 13:
        //or.gt.s
        *s->tr |= sl > sr;
        break;
        case 14:
        //and.gteq.s
        *s->tr &= sl >= sr;
        break;
        case 15:
        //or.gteq.s
        *s->tr |= sl >= sr;
        break;
        default:
        return;
    }
}


void op8_single_arg(lightvm_state* s, uint16_t op, uint8_t* arg2)
{
    unsigned int secondary=(op & 0xF000) >> 12;
    switch(secondary)
    {
        case 0:
        //push
        lightvm_push(s, *arg2);
        break;
        case 1:
        //pop
        *arg2 = lightvm_pop(s);
        break;
        case 2:
        //not
        *arg2 = ~(*arg2);
        break;
        case 3:
        //unused
        break;
        case 4:
        //unused
        break;
        case 5:
        //unused
        break;
        case 6:
        {
            xcall_t call=lightvm_get_xcall(s, *arg2);
            if(call!=NULL)
            {
                call(s, *arg2);
            }
        }
        break;
        //xcall?
        case 7:
        //and.eq
        *s->tr &= *s->cr == *arg2;
        break;
        case 8:
        //or.eq
        *s->tr |= *s->cr == *arg2;
        break;
        case 9:
        //and.neq
        *s->tr &= *s->cr != *arg2;
        break;
        case 10:
        //or.neq
        *s->tr |= *s->cr != *arg2;
        break;
        case 11:
        //set TR
        *s->tr = 1;
        break;
        case 12:
        //reset TR
        *s->tr = 0;
        break;
        case 13:
        //load CR
        *s->cr = *arg2;
        break;
        case 14:
        //8.bank PROBABLY NOT
        case 15:
        //unused
        default:
        return;
    }
    return;
}


