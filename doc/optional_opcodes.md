#Optional Opcodes

There are a number of optional opcodes which can be enabled by a lightvm configuration option. These are implemented as xcalls to avoid cluttering the primary opcode space.

# MUL/DIV support

Multiply and divide is optional in LightVM, considering it is expensive for some architectures to support. 

Compiler option: SINGLE_MUL_DIV_SUPPORT

## single-wide unsigned multiply (wmul)

operation: r0*r1=r2

## single-wide signed multiply (wsmul)

operation: r0*r1=r2

## single-wide unsigned divide(wdiv)

operation: r0/r1=r2

## single-wide signed divide(wsdiv)

operation: r0/r1=r2

## sinlge-wide unsigned modulo(wmod)

operation: r0%r1=r2

(signed modulo is weird and implementation defined in C.. so lets just leave it out)

# double-wide MUL/DIV support

Double wide operations operate on 32 bit numbers 

Compiler option: DOUBLE_MUL_DIV_SUPPORT (also requires SINGLE support)


