#New RISC Design

To facilitate a nice lean implementation, I am going with a potentially slower, but smaller and easier to implement RISC design for opcodes. 

#Opcode Format

Each opcode is either 2 bytes or 4 bytes and follows a very standardized format. 

Each bit (with 15 being most significant and 0 being least. If your markdown viewer does what mine does. 1 is most significant and first)

15. 8-bit operations
14. Destination/Source 1 is indirect
13. Source 2 is indirect
12. Source 2 is immediate (means the opcode is 4 bytes. The last 2 bytes are the immediate argument
11. Primary Opcode
10. Primary Opcode
9. Primary Opcode
8. Primary Opcode
7. Destination register/Secondary opcode(for single argument opcodes)
6. Destination register/Secondary opcode(for single argument opcodes)
5. Destination register/Secondary opcode(for single argument opcodes)
4. Destination register/Secondary opcode(for single argument opcodes)
3. Destination/Source 1/Source 2 register (1st arg for single argument opcode. 2nd arg for double argument opcodes)
2. Destination/Source 1/Source 2 register (1st arg for single argument opcode. 2nd arg for double argument opcodes)
1. Destination/Source 1/Source 2 register (1st arg for single argument opcode. 2nd arg for double argument opcodes)
0. Destination/Source 1/Source 2 register (1st arg for single argument opcode. 2nd arg for double argument opcodes)

With this set of operations in mind, you can already foresee how there might be some counter-intuitive opcodes. I leave everything "legal" at implementation level, but you should be smart enough to know what not to do. Not all bits will be used for all opcodes. 

## Arguments

Just in case it's not clear, arguments are passed with the set 4 bits for each argument (or 16 in the case of immediate). 
Then, 1 bit for each argument (either 14 or 12) determines as to if the argument is indirect and goes to memory. 
ie, determines `r0` vs `[r0]`

# Primary opcode list

0. 1-arg opcodes (see secondary for "actual" action)
1. xor
2. or
3. and
4. xcall
5. mv (move)
6. Comparison 1-arg opcodes(see secondary for actual action)
7. mv.iftrue
8. shl (shift left)
9. shr (shift right)
10. add
11. sub
12. branch.short/push.mv **special!**
13. branch.short.iftrue/push.mv.iftrue **special!**
14. branch.short.iffalse/push.mv.iffalse **special!**
15. Extensions **special!**

Now, to annotate what I mean with these special opcodes. A special opcode breaks the standard format. So, for branch.short and branch.short.iftrue, These are done when 8-bit is true. To facilitate easy and efficient short branches, the second byte which would normally give the arguments is instead used as a relative (signed) address to add to IP. This is a special case in that IP is hard coded. you can't use branch.short for anything other than changing IP. When it's not 8-bit ops, this opcode is push.mv. This seemed like a useful thing to generalize. So, you can do a `call`, where you push IP and then change it. You can also do save a register before modifying it. push.mv is not special in any way, but there is not an 8-bit version for it. 

For Extensions, the format is a free for all. There is no assumption about what format the second byte is. If a 4-byte opcode is needed however, the Immediate flag should be set to tell the fetcher to get 4 bytes instead of 2. 

# 1-Argument Opcodes

0. push
1. pop
2. not
3. **unused**
4. **unused**
5. **unused**
6. **unused**
7. and.eq
8. or.eq
9. and.neq (not equal)
10. or.neq
11. set (TR)
12. reset (TR)
13. load CR (only "useful" for 8-bit operations. Will clear CR and only load bottom 8-bits in 8-bit mode)
14. **unused**
15. **unused**


# Comparison 1-arg opcodes

0. and.lt (less than)
1. or.lt
2. and.lteq (less than or equal)
3. or.lteq
4. and.gt (greater than)
5. or.gt
6. and.gteq (greater than or equal)
7. or.gteq
8. and.lt.s (less than) (the .s suffix means signed comparison)
9. or.lt.s
10. and.lteq.s (less than or equal)
11. or.lteq.s
12. and.gt.s (greater than)
13. or.gt.s
14. and.gteq.s (greater than or equal)
15. or.gteq.s

Note: In 8-bit mode, signed comparisons take place disregarding the top 8-bits of CR. So, 0x00FF is considered -1 when in 8-bit mode.


# Arguments for 1-argument opcodes

For 1-argument opcodes, the first "argument" is basically ignored. The only argument to 1-argument opcodes is the second. This means that single argument opcodes can use register, register indirect, immediate, and immediate indirect. Note that not every combination is considered "valid". It makes no sense to `pop 100`, even if it is possible. The result of such an operation would be nothing. It'd go into an invisible "temp register" used to hold immediate opcodes. 

# Unused space in immediate opcodes

In immediate opcodes, the area typically used for the second argument register number is unused. This 16 bit area should be set to 0. 
This area may be used for future extended opcode formats or something like that and to prevent your code from breaking future compatibility, you should leave it at 0.
