# Design Goals

These are the design goals of LightVM

* Work **everywhere**. From the 8-bit micro with less than 1K of RAM to your 64-bit PC
* Small implementation. As can be gathered from the opcode design, the easier to implement, the better off we are
* As fast and resource thin as possible while obeying the top two goals. That being said, hand-coded assembly optimizations for certain platforms aren't going to happen (without pull requests).

# Registers

All registers are considered "general purpose" However, the top 4 registers have special purposes and special opcodes which act on only them. They can be used for other purposes (except for IP in 99% of cases), but unless you're doing some crazy optimizing it's probably not the best thing to do.

* r15: IP -- Instruction Pointer. Points to what instructions are currently executing
* r14: SP -- Stack Pointer. Points to the top of the stack (the stack "grows" down).
* r13: TR -- Truth Register. Is 0 for false for conditional opcodes. Is greater than 0 for true. 
* r12: CR -- Compare Register. Holds the current value being compared against. See also [[Compares]]
