# XCALL opcode

XCALL is a way to make calls outside of the VM and stands for eXternal CALL. 

# Calling convention

Arguments should fit within 16-bytes. This is so that all of the arguments could be pointed at memory address 0 and then use the convenient register memory area. If you need a block of memory, it should be passed with an address and a count. If you need byte instead of word arguments, you should ensure all of your arguments still fit into 16-bytes. This is because using 8-bit mode, the lower 8 registers are exposed as 16 registers, hence still being 16-bytes.

Arguments double as return values, where needed. Return values should use space set aside for arguments, unless not possible. You should strive to avoid requiring more memory space just so that return values don't overwrite arguments 

# Notation

Because arguments should fit into the bottom 8 registers, we will refer to them as registers, even if they aren't necessarily. 

# How it works from the VM

Let's say you have a `print` xcall at xcall #0. You'd do something liek this:

    const PRINT 0
    mv r0, mystring
    xcall PRINT
    ...
    mystring: db 'foo bar', 0

# How it works from outside the VM

Xcalls use a callback system, so when `xcall 0` is executed, lightvm will call your callback that is assigned to 0. There are two options for how this is actually kept track of. There is a memory-heavy(but fast) array option (stored in lightvm_state) or there is the slower, but memory-thin linked list option. 

Whichever one suits your platform best can be configured in config.h


# Special xcalls

* 0x8000-0x800F is used for the check-xcall-status call. 
** 0x8000+register number for the source and destination. The source register will be overwritten
** Returns the number of Xcalls assigned to this slot (either 1 or 0 at the moment, but could be more than 1 in the future)
* 0x8010-0x801F is puts 
** 0x8010+register should point at the string to print's address in memory


#Opcode extensions

0xF000 and above is reserved for opcode extensions. 

#"Wide" Xcall format

When an xcall is expected to be often used and wants to make a return value able to go into any register, it can be made a "wide" xcall by occupying 16 Xcall slots.

For example, the "check-xcall-status" occupies 16 slots. 
0 is the base call, and then add to that which register to use for both the source and destination (it is overwritten). 
 



