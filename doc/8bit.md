# 8-bit operations

When 8-bit operations is chosen, everything magically becomes 8-bit, with exceptions. 

# 8-bit registers:

Instead of arbitrarily choosing either low or high bytes of registers to use, when using 8-bit operations, the bottom 8 registers are "exploded" into 16 8-bit registers. This means you can't do stuff like modifying IP or CR with 8-bit operations, but why would you want to do that anyway?

# Explanation Of Operation

This machine is Turing complete without 8-bit operations. The primary awesome factor of including 8 bit operations though is for easy handling of bytes and ASCII strings. If you want to add 2 bytes together and return the result as a byte you can do this:

    mv.8 r0a, 0x7F
    mv.8 r0b, 0x90
    add.8 r0a, r0b

Whereas with only 16-bit mode, you'd have to pay attention to overflows and such and you'd either be doing extra work to pack 2 bytes into each register or you'd wasting a byte in your registers. 

In 8-bit mode, registers are 8-bit, comparisons are 8-bit (using the lower 8-bits of CR), pointers are 16-bit but point to 8-bit values. Indirect registers use the full 16-bit register for the pointer, but then point to a byte. 


# Immediates

Immediate operands are still 16-bits. This means a byte will be wasted in 8-bit mode. However, to keep a good 4/2 byte alignment, I think this is OK. Immediate bytes should be rare. Note, immediate pointers of course still work and will properly load a byte, however useful that is. 

# Optional-ish

8-bit operations is by no means required for a good virtual machine. So, this is being made "sort of" optional for very small implementation constraints. However, I'll be writing programs assuming it exists. The only required thing around 8-bit operation that would be hard to live without is branch.short


