# Comparisons

Comparisons and conditionals work quite a bit different than other CPU architectures. Despite this, I think this implementation is pretty awesome. Instead of messing with things like `carry` and `overflow` flags, we use much higher level operations like `is this greater than that` and if so, then TR is set to true, otherwise it's set to false.

But, there is actually even more goodness to it than that. Let's write an example program:

    mv CR, 20 #load our comparison register
    resetTR #reset TR to false just in case 
    or.lt 50 #TR = TR or CR < 50;
    and.gt 10 #TR = TR and CR > 10
    br.s.iftrue foo #branch to label foo if TR is true
    #throw some error
    foo:
    #do something successful

As you can see, this is some fairly concise bytecode to handle common range comparisons. The real power emerges when you swap out CR for other conditions and check more than one variable in this manner. Of course, this has the ability to be a bit slower due to slightly redundant execution. If you're executing 20 conditional opcodes before branching to error, you're going to have a slow time. 


