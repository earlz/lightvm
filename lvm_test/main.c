#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "../src/lightvm.h"
#include "lvm_test.h"


const int MEMORY_SIZE=32*1024;

volatile int stop_lvm=0;

int main(int argc, char** argv){
    if(argc<2){
        printf("usage: lvm_test file\n");
        printf("file should be a self contained raw memory image to load into ligthvm\n");
        return 0;
    }
    
    uint8_t* memory=malloc(MEMORY_SIZE);
    if(memory==NULL){
        printf("could not allocate memory buffer");
        return -1;
    }
    FILE* file=fopen(argv[1], "rb");
    
    fseek(file, 0L, SEEK_END);
    int size = ftell(file); //get file size
    if(size > MEMORY_SIZE){
        printf("File is larger than available memory, %i bytes", MEMORY_SIZE);
        fclose(file);
        free(memory);
        return -1;
    }
    //seek back to beginning
    fseek(file, 0L, SEEK_SET);  
    
    printf("loading %i bytes into memory buffer of size %i bytes\n", size, MEMORY_SIZE);
    
    for(int i=0;i<size;i++){
      memory[i]=fgetc(file);  
    }
    
    lightvm_state state;
    state.memory=memory;
    state.memorysize = MEMORY_SIZE;
    lightvm_init(&state);
    add_all_xcalls(&state);
    
    while(!stop_lvm){
        lightvm_step(&state);
    }
    
    
    fclose(file);
    free(memory);   
    return 0;
}

