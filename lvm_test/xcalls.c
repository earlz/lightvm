#include <stdio.h>
#include <stdlib.h>
#include "lvm_test.h"

void add_all_xcalls(lightvm_state* s){
    lightvm_add_xcall(s, XCALL_STDLIB, stdlib_xcall);
    lightvm_add_xcall(s, XCALL_STDIO, stdio_xcall);
}

void stdlib_xcall(lightvm_state* s, int num){
    int r0=*lightvm_register(s, 0);
    switch(r0){
        case 0:
            printf("received exit xcall\n");
            stop_lvm=1;
            break;
        case 0xFFFF:
            lightvm_dump(s);
            break;
        default:
            printf("unknown xcall for stdlib. r0=%i\n",r0);
            break;
    }
}

void stdio_xcall(lightvm_state* s, int num){
    printf("stdio\n");
    
}
