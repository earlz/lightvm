#ifndef LVM_TEST_H
#define LVM_TEST_H

#include "../src/lightvm.h"

#define XCALL_STDLIB 0
#define XCALL_STDIO 1

void stdio_xcall(lightvm_state* s, int num);
void stdlib_xcall(lightvm_state* s, int num);
void add_all_xcalls(lightvm_state* s);

extern volatile int stop_lvm;

#endif